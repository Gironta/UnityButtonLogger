UnityButtonLogger

Dependencies
Place all required build dependencies in a lib directory in the project's root.

Build
dotnet build -c Release

Install
Install like any other BepInEx plugin.

Supports
The engine version specified in csproj, probably later versions as well.
